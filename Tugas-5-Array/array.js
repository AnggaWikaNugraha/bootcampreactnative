function range(a, b) {
  var hasil = [];
  if (a < b) {
    for (let i = a; i <= b; i++) {
      hasil.push(i);
    }
  } else if (a > b) {
    for (let i = a; i >= b; i--) {
      hasil.push(i);
    }
  } else if (a == null || b == null) {
    hasil += -1;
  }
  return hasil;
}
console.log(">>>>>>>> soal 1 <<<<<<<<<<<")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
// console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
// console.log(range(54, 50)) // [54, 53, 52, 51, 50]
// console.log(range()) // -1

console.log(">>>>>>>> soal 2 <<<<<<<<<<<");

function rangeWithStep(startNum, stopNum, step) {
  var result = [];

  if (typeof stopNum == "undefined") {
    // one param defined
    stopNum = startNum;
    startNum = 0;
  }

  if (typeof step == "undefined") {
    step = 1;
  }

  if ((step > 0 && startNum >= stopNum) || (step < 0 && startNum <= stopNum)) {
    for (var i = startNum; step > 0 ? i > stopNum : i < stopNum; i -= step) {
      result.push(i);
    }
  }

  for (var i = startNum; step > 0 ? i < stopNum : i > stopNum; i += step) {
    result.push(i);
  }
  return result;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
// console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
// console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
// console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


console.log(">>>>>>>> soal 3 <<<<<<<<<<<");

function sum(startNum, stopNum, step) {
  var result3 = [];
  var result3jumlah = 0;

  if (typeof stopNum == "undefined") {
    // one param defined
    stopNum = startNum;
    startNum = 0;
  }

  if (typeof step == "undefined") {
    step = 1;
  }
  if ((step > 0 && startNum >= stopNum) || (step < 0 && startNum <= stopNum)) {
    for (var i = startNum; step > 0 ? i > stopNum : i < stopNum; i -= step) {
      result3.push(i);
    }
  }
  
  for (var i = startNum; step > 0 ? i < stopNum : i > stopNum; i += step) {
    result3.push(i);
  }

  if(result3.length > 0){
    result3jumlah = result3.reduce((a,b) => a + b)
  }
  return result3jumlah;
}

// Code di sini
console.log(sum(1,10)) // 55
// console.log(sum(5, 50, 2)) // 621
// console.log(sum(15,10)) // 75
// console.log(sum(20, 10, 2)) // 90
// console.log(sum(1)) // 1
// console.log(sum()) // 0 


console.log(">>>>>>>> soal 4 <<<<<<<<<<<");

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function arrayToObjek (arrays){
  var hasilArrauToObjek = '';
  for( let i = 0; i < arrays.length; i++){
    for(let j=0 ;j<i ; j++){
      hasilArrauToObjek += " Nomor ID : " + arrays[i][j] + " \n Nama Lengkap: " + arrays[i][j] + arrays[i][j] + " \n TTL: " + arrays[i][j] + " \n Hobi: " + arrays[i][4] + "\n\n"; 
    }
  }
  return hasilArrauToObjek;
}

var jawaban2 = arrayToObjek(input);
console.log(jawaban2)

var tampung = "";
function balikString(kalimat){
  for(let i = kalimat.length -1; i >= 0 ; i--){
    tampung += kalimat[i]
  }
  return tampung
}

console.log(">>>>>>>>>>>>>>>>> 5 <<<<<<<<<<<<<")

var tampung = "";
function balikKata(kalimat){
  for(let i = kalimat.length -1; i >= 0 ; i--){
    tampung += kalimat[i]
  }
  return tampung
}

// console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
// console.log(balikKata("Haji Ijah")) // hajI ijaH
// console.log(balikKata("racecar")) // racecar
// console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log(">>>>>>>>>>>>>>>>> 6 <<<<<<<<<<<<<")
