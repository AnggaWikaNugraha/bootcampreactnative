console.log(">>>>>>>>>>>> soal 1 <<<<<<<<<<<<<<<<<<");
function arrayToObject(arr) {
  // Code di sini
  const nowTahun = new Date().getFullYear();
  const age1 = nowTahun - arr[0][3];
  const age2 = nowTahun - arr[1][3];

  console.log(
    `${arr[0][0]} ${arr[0][1]}: {firstname : "${arr[0][0]}" , lastname : "${arr[0][1]}", gender: "${arr[0][2]} , age : "${age1}"}`
  );
  console.log(
    `${arr[1][0]} ${arr[1][1]}: {firstname : "${arr[1][0]}" , lastname : "${arr[1][1]}", gender: "${arr[1][2]}"`
  );
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

console.log(">>>>>>>>>>>> soal 2 <<<<<<<<<<<<<<<<<<");
function shoppingTime(memberId, money) {
  const case1 = {
    memberId: "1820RzKrnWn08",
    money: 2475000,
    listPurchased: {
      "Sepatu Stacattu" : 1500000,
      "Baju Zoro" : 500000,
      "Baju H&N" : 250000,
      "Sweater Uniklooh" : 175000,
      "Casing Handphone" : 50000,
    },
    changeMoney: 0,
  };

  let money1 = (case1.listPurchased["Sepatu Stacattu"] + case1.listPurchased["Baju Zoro"] + case1.listPurchased["Baju H&N"] + case1.listPurchased["Sweater Uniklooh"] + case1.listPurchased["Casing Handphone"]) - money

  const case2 = {
    memberId: "82Ku8Ma742",
    money: 170000,
    listPurchased: ["Casing Handphone"],
    changeMoney: 120000,
  };

  return `{member id : '${memberId}', money : ${money}, listPurchased: [
    "Baju Zoro",
    "Sweater Uniklooh",
  ], changeMoney : ${money1}`
}
console.log(shoppingTime("1820RzKrnWn08", 2475000));

console.log(">>>>>>>>>>>> soal 3 <<<<<<<<<<<<<<<<<<");

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  let org = [
    {"penumpang" : arrPenumpang[0][0] , "naik dari" : arrPenumpang[0][1], "tujuan" : arrPenumpang[0][2], "bayar" : arrPenumpang[0][3]},{"penumpang" : arrPenumpang[1][0], "naik dari" : arrPenumpang[1][1], "tujuan" : arrPenumpang[1][2], "bayar" : arrPenumpang[1][3]}
  ]
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]