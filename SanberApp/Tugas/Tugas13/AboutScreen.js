import React from 'react'
import { StyleSheet, View , Text} from 'react-native';
import { Feather } from '@expo/vector-icons';
const AboutScreen = () => {
  return (
    <View style={styles.page}>
      <View style={styles.lingkaran}>
        <Feather name="user" size={24} color="black" />
      </View>
      <Text style={styles.text('nama')}>Achand Himy</Text>
      <Text style={styles.text('')}>React native Developer</Text>
      {/* <View>
        <View style={styles.containerDetail}>
          <Text>Portfolio</Text>
        </View>
        <View style={styles.containerDetail}>
          <Text>Contact</Text>
        </View>
      </View> */}
    </View>
  )
}

export default AboutScreen

const styles = StyleSheet.create({
  page : {
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center'
  },
  lingkaran : {
    width : 150,
    height :150,
    borderRadius : 150/2,
    backgroundColor : 'gray',
    display : 'flex',
    justifyContent : 'center',
    alignItems: 'center',
    
  },
  text : type =>  ({
    textAlign :'center',
    fontSize : type == 'nama' ? 18 : 22,
    marginVertical : 10,
  }),
  containerDetail : {
    display : 'flex',
    alignItems : 'flex-start'
  }
})