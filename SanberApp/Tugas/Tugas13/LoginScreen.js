import React from 'react'
import { Image, View, Text, TextInput, StyleSheet, Button, TouchableOpacity } from 'react-native'
import Logo  from './images/logo.png'

const LoginScreen = () => {
  return (
    <View style={styles.page}>
      <Image source={Logo}/>
      <Text style={styles.judul}>Login</Text>
      <View style={styles.container__formgroup}>
       <Text style={styles.label}>Username</Text>
        <TextInput style={styles.input}/>
      </View>
      <View style={styles.container__formgroup}>
       <Text style={styles.label}>Password</Text>
        <TextInput style={styles.input}/>
      </View>
      <View style={styles.containerBottom}>
        <TouchableOpacity style={styles.button ('primary')}>
          <Text style={styles.textButton('primary')}>Daftar</Text>
        </TouchableOpacity>
        <Text>Atau</Text>
        <TouchableOpacity style={styles.button('')}>
          <Text style={styles.textButton('')}>Masuk</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default LoginScreen

const styles = StyleSheet.create({
  page : {
    flex : 1,
    justifyContent : 'flex-start',
    padding: 70,
  },
  label : {
    fontSize : 12,
    color : '#003366',
    fontWeight : '700'

  },
  input : {
    borderWidth:1,
    borderColor : '#003366',
    borderRadius : 5,
    padding :12
  },
  containerBottom : {
    display: 'flex',
    alignItems : 'center',
  },
  button : type =>  ({
    borderRadius : 10,
    backgroundColor : type == 'primary' ? '#003366' : 'white',
    paddingTop : 10,
    paddingBottom : 10,
    paddingLeft : 30,
    paddingRight : 30,
    borderWidth : type == 'primary' ? 0 : 1,
    borderColor : type == 'primary' ? '' : '#003366',
    marginTop : 20,
    marginBottom : 20
    
  }),
  textButton : type =>  ({
    color : type == 'primary' ? 'white' : '#3EC6FF',
    fontSize : 18
  }),
  judul : {
    textAlign:'center',
    fontSize : 24,
    fontWeight: "bold",
    color : '#003366',
    marginTop : 70,
    marginBottom : 40
  },
  container__formgroup : {
    marginBottom : 10,
    marginTop : 10
  }
})
