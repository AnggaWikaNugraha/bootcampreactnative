console.log('---------------------------- soal 1')
// soal 1
console.log("LOOPING PERTAMA")
let x = 2
while(x < 22) {
  console.log(x + " - I Love coding")
  x+=2;
}
console.log('LOOPING KEDUA')
let y = 20
while (y > 0) {
  console.log(y + " - I will become a frontend developer")
  y-=2;
}

// soal 2
console.log('---------------------------- soal 2')
for(i = 1 ; i <= 20; i++){
  if (i % 2 == 0 ) {
    console.log(i + ' - berkualitas')
  }
  if (i % 2 != 0 && i % 3 !== 0 ) {
    console.log(i + ' - santai')
  }
  if (i % 3 === 0 && i % 2 != 0) {
    console.log( i + " - I love coding")
  }
}


// soal 3
console.log('---------------------------- soal 3')

let hasil =''
for(let i=0;i<4;i++){
  for(let j=0; j<8; j++){
    hasil += "#"
  }
  hasil += '\n'
}
console.log(hasil)


// soal 4
console.log('---------------------------- soal 4')
var hasil4 = ''
for(let i = 1; i<8; i++){
  for(let j = i; j>=1; j--){
    hasil4 += "#"
  }
  hasil4 += '\n'
}
console.log(hasil4)


// soal 5
console.log('---------------------------- soal 5')
let s = '';
let panjang = 10;
let lebar = 10;

for(let i=1 ; i <= lebar; i++){
  if(i % 2 == 0){
    for(let j= 1; j <= panjang; j++){
      if(j % 2 == 0){
        s += '#';
      }else{
        s += ' ';
      }
    }
  }else{
    for(let j =1;j <= panjang; j++){
      if(j % 2 == 0){
        s += ' ';
      }else{
        s += '#'
      }
    }
  }
  s += '\n'
}
console.log(s)