// if-else
console.log('>>>>>>>>>>>>>>>if else<<<<<<<<<<<<<<<<<')
var nama = "Jane";
var peran = "Penyihir";

if(nama == "" && peran == ""){
  console.log('nama harus diisi')
}
else if(nama == "john" || nama == "John"&& peran == "" ){
  console.log(`hallo ${nama}, Pilih peranmu untuk memulai game`)
}
else if(nama == "jane" || nama == "Jane"&& peran == "Penyihir" || peran == "penyihir" ){
  console.log(`selamat datang di Dunia warewolf, ${nama}.`)
  console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa pemain warewolf`)
}
else if(nama == "jenita" || nama == "Jenita"&& peran == "Guard" || peran == "guard" ){
  console.log(`selamat datang di Dunia warewolf, ${nama}.`)
  console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf`)
}
else if(nama == "junaedi" || nama == "Junaedi"&& peran == "werewolf" || peran == "Werewolf" ){
  console.log(`selamat datang di Dunia warewolf, ${nama}.`)
  console.log(`Halo ${peran} ${nama}, kamu akan memakan mangsa setiap malam`)
}

// swith case
console.log('>>>>>>>>>>>>>>>swith case<<<<<<<<<<<<<<<<<')
var hari = 1;
var bulan = 5;
var tahun = 1945;

switch(bulan){
    case 1: bulan = 'Januari'; break;
    case 2: bulan = 'Februari'; break;
    case 3: bulan = 'Maret'; break;
    case 4: bulan = 'April'; break;
    case 5: bulan = 'Mei'; break;
    case 6: bulan = 'Juni'; break;
    case 7: bulan = 'Juli'; break;
    case 8: bulan = 'Agustus'; break;
    case 9: bulan = 'September'; break;
    case 10: bulan = 'Oktober'; break;
    case 11: bulan = 'November'; break;
    case 12: bulan = 'Desember'; break;
}

var tanggal = `${hari} ${bulan} ${tahun}`;
console.log(tanggal);